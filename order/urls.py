from django.urls import path

from order.views import OrderViewNoSer, OrderView

urlpatterns = [
    path('', OrderView.as_view({'post': 'create'})),
    path('no_ser', OrderViewNoSer.as_view({'post': 'create'})),
]

{
    "address": "Чуй 51",
    "order_products": [
        {
            "product_id": 1,
            "count": 3
        },
        {
            "product_id": 2,
            "count": 1
        }
    ]
}