from rest_framework import serializers

from order.models import Order


class OrderCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ('address', )

    def create(self, validated_data):
        user = self.context.get('request').user.profile
        order = Order.objects.create(client=user, **validated_data)
        return order
